
// - Считать с помощью модельного окна браузера данные пользователя: имя и возраст.
// - Если возраст меньше 18 лет - показать на экране сообщение: `You are not allowed to visit this website`.
// - Если возраст от 18 до 22 лет (включительно) - показать окно со следующим сообщением: `Are you sure you want to continue?` и кнопками `Ok`, `Cancel`. Если пользователь нажал `Ok`, показать на экране сообщение: `Welcome, ` + имя пользователя. Если пользователь нажал `Cancel`, показать на экране сообщение: `You are not allowed to visit this website`.
// - Если возраст больше 22 лет - показать на экране сообщение: `Welcome, ` + имя пользователя.
//- После ввода данных добавить проверку их корректности. Если пользователь не ввел имя, либо при вводе возраста указал не число - спросить имя и возраст заново (при этом значением по умолчанию для каждой из переменных должна быть введенная ранее информация).


let userName = prompt('Введите ваше имя')
let userAge = +prompt('Сколько вам лет?')
while(!userName || Number.isNaN(userAge) ) {
    userName = prompt('Введите имя повторно', 'vladimir')
    userAge = +prompt('Введите сколько вам лет числом!', '20')
   
}
if(userAge<18) {
    document.write("You are not allowed to visit this website");
} else if (userAge >= 18 && userAge <=22) {
   userContinue = confirm('Are you sure you want to continue?')
    if(userContinue === true) {
        document.write(`Welcome, ${userName}`);
    } else {
        document.write("You are not allowed to visit this website");
    }
} else if (userAge>22) {
    document.write(`Welcome, ${userName}`);
}
